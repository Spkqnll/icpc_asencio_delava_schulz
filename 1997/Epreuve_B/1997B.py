def main():
    routes = 0
    nb_Routes = int(input('Entrez le nombre de route(s) : '))
    while nb_Routes > 0:
        s = int(input('Entrez nombre d arret(s) sur la route {} : '.format(nb_Routes)))
        tmp = 0
        debut = 0
        fin = 0
        note = 0
        precedente = 0
        i = 0
        for i in range(s):
            x = int(input('Entrez la note de l arret {} : '.format(i)))
            note += x
            if note < 0:
                note = 0
                tmp = i+1
            if note < precedente:
                tmp = i
            if note >= precedente:
                if note > precedente or (note == precedente and (i - tmp > fin - debut)):
                    debut = tmp
                    fin = i
            precedente = note
        i += 1
        routes += 1
        if precedente > 0:
            print("The nicest part of route {} is between stops {} and {}".format(routes, debut, fin))
        else:
            print("Route {} has no nice parts".format(routes))
        nb_Routes -= 1

main()