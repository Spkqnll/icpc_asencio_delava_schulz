#!/usr/bin/env python3

import sys

sys.setrecursionlimit(int(1e6))  #probleme recursivite sur macOS

prix_minimal= []
def go_to_destination(capacite, consomation, distance_dest, distance_parcouru):
    distance_possible = consomation*capacite + distance_parcouru
    print(distance_possible)
    if distance_possible<=float(distance_dest):
        return False
    else :
        return True

def calcul_cout_min(prix, liste_station, consomation, capacite, nb_station, distance_parcouru, distance_dest):
    if go_to_destination(capacite, consomation, distance_dest, distance_parcouru):
        
        prix_minimal.append(prix)
    else :
        for i in range(nb_station):
            print(i)
            if liste_station[i].get("distance") < (capacite*consomation) + distance_parcouru:
                print("passe")
                gal_conso = (liste_station[i].get("distance") /consomation)
                cout_station = (gal_conso * liste_station[i].get("prix"))/100 + 2
                print("cout station : " + str(cout_station))
                calcul_cout_min(float(cout_station)+float(prix), liste_station[i:], consomation, capacite, nb_station-i, distance_parcouru + liste_station[i].get("distance"), distance_dest)        

def main():
    f= open("text_A.txt","r")
    distance_max=f.readline()
    print(distance_max)
    capacite, consomation, cout_depart, nb_station = f.readline().split()
    capacite = float(capacite)
    consomation = float(consomation)
    cout_depart = float(cout_depart)
    nb_station = int(nb_station)
    liste_station = []
    for i in range(0,nb_station) :
        distance, prix = f.readline().split()
        liste_station.append({"distance":float(distance), "prix":float(prix)})
    cout = cout_depart
    distance_parcouru = 0
    calcul_cout_min(cout, liste_station, consomation, capacite, nb_station, distance_parcouru, distance_max)        
    print(min(prix_minimal))
if __name__ == '__main__':
    main()