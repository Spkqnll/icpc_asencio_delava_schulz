def position(puzzle):
    for i in range(len(puzzle)):
        for j in range(len(puzzle)):
            if puzzle[i][j] == ' ':
                return i, j

def puzzle():
    erreur = False
    case = 0
    x = 0
    y = 5
    while True:
        puzzle = []
        case += 1
        print("Puzzle #" + str(case) +":")
        mon_fichier = open("fichier.txt", "r")
        lignes = mon_fichier.readlines()[x:y]
        for ligne in lignes:
            puzzle.append(ligne)
        mon_fichier.close()
        mon_fichier = open("fichier.txt", "r")
        sequence_move = mon_fichier.readlines()[y:]
        mon_fichier.close()
        res =""
        for move in sequence_move:
            res += move
        resultat = res.split("0")
        nblignes = resultat[0].count('\n')
        for i in range(len(resultat[0])):
            vide = position(puzzle)
            ligne = vide[0]
            colonne = vide[1]
            move = resultat[0][i]
            if move == "A":
                if ligne == 0:
                    erreur = True
                    break
                else:
                    liste1 = list(puzzle[ligne-1])
                    liste2 = list(puzzle[ligne])
                    liste1[colonne] , liste2[colonne] = liste2[colonne], liste1[colonne]
                    puzzle[ligne-1] = "".join(liste1)
                    puzzle[ligne] = "".join(liste2)
            elif move == "B":
                if ligne == 4:
                    error = True
                    break
                else:
                    liste1 = list(puzzle[ligne+1])
                    liste2 = list(puzzle[ligne])
                    liste1[colonne] , liste2[colonne] = liste2[colonne], liste1[colonne]
                    puzzle[ligne+1] = "".join(liste1)
                    puzzle[ligne] = "".join(liste2)
            elif move == "R":
                if colonne == 4:
                    error = True
                    break
                else:
                    liste1 = list(puzzle[ligne])
                    liste1[colonne+1] , liste1[colonne] = liste1[colonne], liste1[colonne+1]
                    puzzle[ligne] = "".join(liste1)
            elif move == "L":
                if colonne == 0:
                    error = True
                    break
                else:
                    liste1 = list(puzzle[ligne])
                    liste1[colonne-1] , liste1[colonne] = liste1[colonne], liste1[colonne-1]
                    puzzle[ligne] = "".join(liste1)
        if erreur:
            print("  This Puzzle has no final configuration.")
            break
        else:
            for i in range(len(puzzle)):
                print("  ",end='')
                for j in range(len(puzzle)):
                    print(puzzle[i][j], end=' ')
                print("\n", end="")
        x = x + 6 + nblignes
        y = y + 6 + nblignes
        print('\n', end='')
puzzle()