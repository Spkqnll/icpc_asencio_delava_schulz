class Tile:
    def __init__(self, prix, taille):
        self.prix = prix
        self.taille = taille

    def getTaille(self):
        return self.taille
    
    def getPrix(self):
        return self.prix
    
    
def get_data():
    file = open("input.txt", "r")
    number_tiles = int(file.readline())
    front_tiles = []
    back_tiles = []
    prix = file.readline().split()
    taille = file.readline().split()
    for i in range(0,number_tiles):
        back_tiles.append(Tile(int(prix[i]),int(taille[i])))
    prix = file.readline().split()
    taille = file.readline().split()
    for i in range(0,number_tiles):
        front_tiles.append(Tile(int(prix[i]),int(taille[i])))
    return(front_tiles, back_tiles,number_tiles)

def tri_prix(number_tiles, tiles):
    return sorted(range(number_tiles), key=lambda k: tiles[k].getPrix())

def checkMove(back, front, i, front_tiles, back_tiles):
    if(front_tiles[front[i]].getPrix()==front_tiles[front[i-1]].getPrix()):
        if(back_tiles[back[i]].getTaille()>front_tiles[front[i-1]].getTaille() and front_tiles[front[i]].getTaille()<back_tiles[back[i-1]].getTaille()):
            tmp = front[i]
            front[i] = front[i-1]
            front[i-1] = tmp
    elif(front_tiles[front[i]].getPrix()==front_tiles[front[i+1]].getPrix()):
        if(back_tiles[back[i]].getTaille()>front_tiles[front][i+1].getTaille() and front_tiles[front[i]].getTaille()<back_tiles[back[i+1]].getTaille()):
            tmp = front[i]
            front[i] = front[i+1]
            front[i+1] = tmp
    elif(back_tiles[back[i]].getPrix()==back_tiles[back[i-1]].getPrix()):
        if(back_tiles[back[i]].getTaille()>front_tiles[front][i-1].getTaille() and front_tiles[front[i]].getTaille()<back_tiles[back[i-1]].getTaille()):
            tmp = back[i]
            back[i] = back[i-1]
            back[i-1] = tmp
    elif(back_tiles[back[i]].getPrix()==back_tiles[back[i+1]].getPrix()):
        if(back_tiles[back[i]].getTaille()>front_tiles[front][i+1].getTaille() and front_tiles[front[i]].getTaille()<back_tiles[back[i+1]].getTaille()):
            tmp = back[i]
            back[i] = back[i+1]
            back[i+1] = tmp
    else:
        print("impossible")
        exit(0)


def tri_tile(number_tiles, front_tiles, back_tiles):
    back = tri_prix(number_tiles, back_tiles)
    front = tri_prix(number_tiles, front_tiles)
    for i in range(number_tiles):
        if(front_tiles[front[i]].getTaille()>=back_tiles[back[i]].getTaille()):
            checkMove(back, front, i, front_tiles, back_tiles)
    back = [x+1 for x in back]
    front = [x+1 for x in front]
    print(back)
    print(front)

def main():
    front_tiles, back_tiles, number_tiles = get_data()     
    tri_tile(number_tiles, front_tiles, back_tiles)

if __name__ == '__main__':
    main()



    

